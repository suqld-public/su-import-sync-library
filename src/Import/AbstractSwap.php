<?php

namespace SUA\Import;

use Exception;
use ReflectionClass;

/**
 * Abstract class to define the basic structure of import scripts where table switching is utilised.
 */
abstract class AbstractSwap extends AbstractImport
{
    /**
     * Run all steps of the import. This method uses two tables named table_name and table_name_new. For the period that
     * is being synchronised, the data in the table_name_new table is cleared and then imported from the external
     * source. Once all checks have been completed, the table_name and table_name_new tables are swapped, the relevant
     * data is cleared from the table_name_new table (which was the table_name table) and the data is synchronised from
     * the table_name table to the table_name_new table so that both copies are up-to-date.
     *
     * If anything in the import fails, the swap is reversed and the table_name table remains untouched. The data in
     * table_name_new which is then the only table which has been changed is then restored from table_name so the data
     * is as it was before the import started. This ensures that if the synchronisation is run again for a different
     * period of data, corrupt data does not get placed into the table_name table.
     */
    public function run()
    {
        $this->log->info('Starting synchronisation of ' . (new ReflectionClass($this))->getShortName());
        try {
            $this->createTemporaryTables();
            $this->clearExistingRows();
            $this->preImportChecks();
            $this->runSourceExport();
            $this->import();
            $this->postImportChecks();

            $this->em->getConnection()->beginTransaction();
            try {
                $this->swapTables();
                $this->clearExistingRows();
                $this->synchroniseNewRows(); // the swap has been completed, so update table_name_new with new data
                $this->em->getConnection()->commit();
            } catch (Exception $e) {
                $this->log->error('Rolling back after exception occurred during import');
                $this->log->error($e->getMessage());
                $this->synchroniseNewRows(); // the swap has been undone, so restore original data to table_name_new
                $this->em->getConnection()->rollBack();
            }
        } catch (Exception $e) {
            $this->log->error($e->getMessage());
        }
    }

    /**
     * Clear the rows from the table_name_new table for the selected period.
     */
    abstract protected function clearExistingRows();

    /**
     * Switch the table_name and table_name_new tables.
     */
    abstract protected function swapTables();

    /**
     * Synchronise the rows for the selected period from the table_name table to the table_name_new method.
     */
    abstract protected function synchroniseNewRows();
}
