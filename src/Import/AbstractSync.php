<?php

namespace SUA\Import;

use Exception;
use ReflectionClass;

/**
 * Abstract class to define the basic structure of import script where table updating utilised.
 */
abstract class AbstractSync extends AbstractImport
{
    /**
     * Run all steps of the import. Initially data is imported from the external source into temporary tables. A
     * transaction is then started while the synchronisation occurs from the temporary tables to the permanent ones.
     * Errors which occur during this phase will rollback any changes to the permanent tables. Failures which occur
     * during the earlier stages should skip the sync stage altogether.
     */
    public function run()
    {
        $this->log->info('Starting synchronisation of ' . (new ReflectionClass($this))->getShortName());
        try {
            $this->createTemporaryTables();
            $this->preImportChecks();
            $this->runSourceExport();
            $this->import();
            $this->postImportChecks();

            $this->em->getConnection()->beginTransaction();
            try {
                $this->sync();
                $this->em->getConnection()->commit();
            } catch (Exception $e) {
                $this->log->error('Rolling back after exception occurred during import');
                $this->log->error($e->getMessage());
                $this->em->getConnection()->rollBack();
            }
            $this->postSyncChecks();
        } catch (Exception $e) {
            $this->log->error($e->getMessage());
        }
    }

    /**
     * Synchronise (add, update and delete) data from temporary tables into actual tables. This method is wrapped in a
     * transaction block so that all or none of the changes to the permanent tables should occur.
     */
    abstract protected function sync();

    /**
     * Perform checks on the data after it has been imported into the permanent tables. This allows for querying the new
     * permanent data in the MySQL database. This block should not generally be used for import warnings, because the
     * data has already been permanently updated, but can be used to warn of data inconsistencies or data corruption.
     * Issues can be safely created in this method because it is outside the synchronisation transaction block.
     */
    protected function postSyncChecks()
    {
    }
}
