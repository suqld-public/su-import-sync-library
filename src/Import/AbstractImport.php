<?php

namespace SUA\Import;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use SUA\Logging\StepLoggerInterface;

/**
 * Abstract class to define the basic structure of MySU import script. All new import scripts should inherit from this.
 */
abstract class AbstractImport
{
    /** @var StepLoggerInterface */
    protected $stepLogger;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var Connection */
    protected $dbh;

    /** @var LoggerInterface */
    protected $log;

    /** @var string */
    protected $table;

    /**
     * @param StepLoggerInterface    $stepLogger
     * @param EntityManagerInterface $em
     * @param LoggerInterface        $log
     */
    public function __construct($stepLogger, $em, $log)
    {
        $this->stepLogger = $stepLogger;
        $this->em = $em;
        $this->dbh = $em->getConnection()->getWrappedConnection();
        $this->log = $log;
    }

    /**
     * Run all steps of the import. This should be done to minimize downtime, maximise efficiency and to utilise
     * rollbacks wherever possible.
     */
    abstract public function run();

    /**
     * Create temporary tables to import data into.
     */
    abstract protected function createTemporaryTables();

    /**
     * Perform checks on data in source location before starting import into temporary tables. If the source data can be
     * easily verified, then this is the least expensive time to verify things because an exception here will exit the
     * process before the import to temporary tables begins. Issues can be safely created in this method because it is
     * outside the synchronisation transaction block.
     */
    protected function preImportChecks()
    {
    }

    /**
     * Start export of data from source system before the import from that system begins. This is useful if a process
     * needs be run to export data before it is ready to be downloaded by the import() method.
     */
    protected function runSourceExport()
    {
    }

    /**
     * Import data from external source into temporary tables.
     *
     * @throws Exception
     */
    abstract protected function import();

    /**
     * Perform checks on the data after it has been imported into temporary tables. This allows for querying the data in
     * MySQL to do verification using a more familiar structure than might be used in the external source. Exceptions
     * here will exit the sync process before the transaction is started and any permanent tables are changed. Issues
     * can be safely created in this method because it is outside the synchronisation transaction block.
     */
    protected function postImportChecks()
    {
    }

    /**
     * @return string
     */
    protected function getImportTable()
    {
        if (is_null($this->table)) {
            throw new RuntimeException('this->table was not set before trying to import a table');
        }

        if (substr($this->table, -1, 1) === '`') {
            return substr($this->table, 1, -1) . '_import';
        }

        return $this->table . '_import';
    }

    /**
     * @param resource      $tempFile
     * @param callable|null $callable
     *
     * @return resource
     */
    protected function cleanupCsv($tempFile, callable $callable = null)
    {
        $this->log->info('Cleanup of csv starting...');
        /*
         * This while loop processes the downloaded CSV one row at a time, processes the row
         * (if a callable function was provided) and then outputs the processed row into a
         * temporary local CSV file. By doing things one row at a time we avoid running out
         * of memory when processing large data sets.
         */
        $out = tmpfile();
        $i = 0;
        while (($row = fgetcsv($tempFile)) !== false) {
            $row = $callable ? $callable($row) : $this->cleanupRow($row);
            $row = array_map('utf8_encode', $row);
            fputcsv($out, $row);
            $i++;

            if ($i % 100000 === 0) {
                $this->log->debug("...row $i");
            }
        }

        $this->log->info("Cleanup of $i rows completed");

        // Closing the file shouldn't be needed, but tmpfiles are being left around so lets close it anyway
        fclose($tempFile);

        // Rewind the file stream to the beginning so that other functions can use it if they want to
        rewind($out);

        return $out;
    }

    /**
     * @param array $row
     *
     * @return array
     */
    protected function cleanupRow($row)
    {
        return $row;
    }

    /**
     * Imports a CSV data source into the provided table.
     *
     * @param resource $stream a file stream pointing to a local CSV file
     * @param string   $table  The MySQL table to import the data into. The database is taken from the class property.
     */
    protected function importCsv($stream, $table)
    {
        $this->log->info('Importing into ' . $table . '...');

        $filename = stream_get_meta_data($stream)['uri'];

        $query = "LOAD DATA LOCAL INFILE ?
        INTO TABLE $table
        CHARACTER SET UTF8
        FIELDS TERMINATED BY ','
        ENCLOSED BY '\"'";

        $stmt = $this->dbh->prepare($query);
        $stmt->execute([$filename]);
        $stmt->closeCursor();

        fclose($stream);
    }
}
