<?php

namespace SUA\Logging;

interface StepLoggerInterface
{
    /**
     * @param float $step
     */
    public function updateStep($step);

    /**
     * @return float
     */
    public function getCurrentStep();
}
